// the lock server implementation

#include "lock_server.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>


lock_server::lock_server():
  nacquire (0)
{
}

lock_protocol::status
lock_server::stat(int clt, lock_protocol::lockid_t lid, int &r)
{
  lock_protocol::status ret = lock_protocol::OK;
  printf("stat request from clt %d\n", clt);
  r = nacquire;
  return ret;
}

lock_protocol::status
lock_server::acquire(int clt, lock_protocol::lockid_t lid, int &r)
{
    pthread_mutex_lock(&lock_mutex);
    map<lock_protocol::lockid_t, lock>::iterator it = lockmap.find(lid);
    if(it != lockmap.end())
    {
        while(it->second.m_status != lock::FREE)
        {
            pthread_cond_wait(&it->second.lcond, &lock_mutex);
        }
        it->second.m_status = lock::LOCK;
        pthread_mutex_unlock(&lock_mutex);
        return lock_protocol::OK;
    }
    else
    {
        lock l(lid, lock::LOCK);
        lockmap.insert(std::make_pair<lock_protocol::lockid_t , lock>(lid, l) );
        return lock_protocol::OK;
    }
}

lock_protocol::status
lock_server::release(int clt, lock_protocol::lockid_t lid, int &r)
{
    pthread_mutex_lock(&lock_mutex);
    map<lock_protocol::lockid_t, lock>::iterator it = lockmap.find(lid);
    if(it != lockmap.end())
    {

    }
}

