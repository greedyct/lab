#ifndef lock_h
#define lock_h

#include <pthread.h>
#include "lock_protocol.h"

class lock
{
public:
    lock(lock_protocol::lockid_t lid, int status) : m_lid(lid), m_status(status) {}
    ~lock() {}
    enum lock_status {LOCK, FREE};
    int m_status;
    lock_protocol::lockid_t m_lid;
    pthread_cond_t lcond;
};

#endif
